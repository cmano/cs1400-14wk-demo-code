# Create a variable so we only have to give the value once

radius = 8

print("Circumference is", 2 * 3.14159 * radius)

# Now do the same for the Area
print("Area is", radius * radius * 3.14159)