# Change the numbers around to try things out.

number = 100
divisor = 3

print("Regular Division")
print(number, "/", divisor, "is", number / divisor)

print()

print("Integer Division")
print(number, "//", divisor, "is", number // divisor)