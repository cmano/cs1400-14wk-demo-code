# Multiple Assignment - multiple assignment operators
print("Multiple Assignment")
valA = valB = valC = 10
print(valA, valB, valC)
print() # Just adding a blank line

# Simultaneous Assignment - single assignment operators
print("Simultaneous Assignment")
valX, valY, valZ = 10, 20, 30
print(valX, valY, valZ)
print()


# Trick with Swap
print("Swap Trick")
num1 = 50
num2 = 100

print(num1, num2)
num1, num2 = num2, num1
print(num1, num2)