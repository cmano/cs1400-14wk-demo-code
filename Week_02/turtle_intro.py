"""
This is simple demo of the turtle module. Note that in demo code I will not comment very much normally. This is so
you can learn by reading the code, not just by reading what the code should be doing. You should comment more than
what you see in the demos.
"""

# turtle is a library module that we want to use, so first we need to import it
import turtle

# The first step is to show the turtle
turtle.showturtle()

# Move the turtle forward 100 places
turtle.forward(100)

# Turn to the right 90 degrees
turtle.right(90)

# Move forward 50 places
turtle.forward(50)

# Change the color
turtle.color("red")

# Draw a circle
turtle.circle(200)

# Move the turtle
turtle.goto(-100, 100)

# Turn drawing off
turtle.penup()

# Move in a circle
turtle.circle(200)

# In PyCharm the program will end automatically, so we can add this to prevent that from happening
turtle.done()