"""
I didn't put comments in here so you can read/modify the code to figure out
what everything means
"""

import turtle

# setup turtle
turtle.showturtle()

# move to start location
turtle.penup()
turtle.goto(-200, -100)

# draw head
turtle.pendown()
turtle.begin_fill()
turtle.fillcolor("yellow")
turtle.circle(100)
turtle.end_fill()

# move to eye location
turtle.penup()
turtle.goto(-240, 0)

# draw eye
turtle.pendown()
turtle.begin_fill()
turtle.fillcolor("black")
turtle.circle(20)
turtle.end_fill()

# move to other eye location
turtle.penup()
turtle.goto(-160, 0)

# draw other eye
turtle.pendown()
turtle.begin_fill()
turtle.circle(20)
turtle.end_fill()

# move to mouth
turtle.penup()
turtle.goto(-260, -40)

# draw mouth
turtle.setheading(315)
turtle.pendown()
turtle.width(10)
turtle.circle(90, 90)

# hide the turtle
turtle.hideturtle()

# cleanup
turtle.done()