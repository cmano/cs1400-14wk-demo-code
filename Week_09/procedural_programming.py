import math


def main():
    circle1 = 5 # This is just data that represents a radius

    print("The area is", calcArea(circle1))
    print("The perimeter is", calcPerimeter(circle1))
    # The data and functions are separate

def calcArea(radius):
    return math.pi * radius ** 2


def calcPerimeter(radius):
    return math.pi * 2 * radius


main()