class Widget:
    def __init__(self, value):
        self.value = value

    # This still works because 'anything' is the first parameter
    # 'self' should be used by convention, not rule
    def getValue(anything):
        return anything.value

    def getPower(self):
        return 2 ** self.value