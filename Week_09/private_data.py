# Note - The class and main() are in the same file here. You should have classes
#           in their own file

# Change the lines that are commented to see the difference between private and public
from math import pi

class Circle:
    def __init__(self, radius):
        # self.radius = radius
        self.__radius = radius

    def getArea(self):
        # return pi * self.radius ** 2
        return pi * self.__radius ** 2


def main():
    circ1 = Circle(10)

    print(circ1.getArea())
    # circ1.radius = 20
    circ1.__radius = 20
    print(circ1.getArea())

main()