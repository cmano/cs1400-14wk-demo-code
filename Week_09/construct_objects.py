from modules.circle import Circle

def main():
    cir1 = Circle(5)
    cir2 = Circle(10)

    print("Circle 1:", cir1.getPerimeter())
    print("Circle 2:", cir2.getPerimeter())

    print("Circle 1:", cir1.getArea())
    print("Circle 2:", cir2.getArea())


main()