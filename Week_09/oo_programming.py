import math


def main():
    # This is an actual Circle object
    circle1 = Circle(5)

    print("The area is", circle1.getArea())
    print("The perimeter is", circle1.getPerimeter())
    # The data and functions are tied together (encapsulated)


class Circle:
    def __init__(self, radius):
        self.__radius = radius

    def getArea(self):
        return math.pi * self.__radius ** 2

    def getPerimeter(self):
        return math.pi * 2 * self.__radius


main()