from math import pi

class Circle:
    def __init__(self, radius):
        self.__radius = radius

    def getArea(self):
        return pi * self.__radius ** 2


def calcArea(radius):
    return pi * radius ** 2


def main():
    circ1 = Circle(10)
    circ2 = Circle(20)

    # Just call the method on the object and the data is there
    print(circ1.getArea())
    print(circ1.getArea())
    print(circ2.getArea())

    # Need to pass in data every time a function is called
    print(calcArea(200))
    print(calcArea(100))
    print(calcArea(100))

main()