import turtle

def main():
    # Create two Turtle objects
    turtle1 = turtle.Turtle()
    turtle2 = turtle.Turtle()

    # Two objects, same type
    print("turtle1:", id(turtle1), type(turtle1))
    print("turtle2:", id(turtle2), type(turtle2))

    # They draw independently!
    turtle1.goto(100, 100)
    turtle2.goto(-100, -100)
    turtle1.circle(50)
    turtle2.circle(-50)

    turtle.done()


main()