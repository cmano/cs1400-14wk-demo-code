from modules.widget import Widget

def main():
    # Anonymous objects can't be used again because it
    # hasn't been assigned to a variable (given a name)
    print(Widget(10).getPower())
    

main()