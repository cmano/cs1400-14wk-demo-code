# Note - The class and main() are in the same file here. You should have classes
#           in their own file

from math import pi

class Circle:
    def __init__(self, radius):
        self.__radius = radius

    def getArea(self):
        return pi * self.__radius ** 2

    def setRadius(self, radius):
        if (type(radius) == int or type(radius) == float) and radius > 0:
            self.__radius = radius

    def getRadius(self):
        return self.__radius


def main():
    circ1 = Circle(10)

    print(circ1.getArea())
    circ1.setRadius(50)
    print(circ1.getArea())

main()