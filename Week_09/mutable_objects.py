# Note - The class and main() are in the same file here. You should have classes
#           in their own file

class Widget:
    def __init__(self, width, height, depth):
        self.width = width
        self.height = height
        self.depth = depth

    def getVolume(self):
        return self.width * self.height * self.depth


def main():
    widget = Widget(50, 20, 5)
    print("Widget:", widget.getVolume())
    useWidget(widget)
    print("Widget:", widget.getVolume())

def useWidget(widget):
    widget.width = widget.width * 0.8
    widget.height = widget.height * 0.6
    widget.depth = widget.depth * 0.5

main()