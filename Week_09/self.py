from modules.widget import Widget

def main():
    num1 = 10
    num2 = 20

    widget1 = Widget(num1)
    widget2 = Widget(num2)

    # Same method name, but on different Widget objects
    # This means the 'self' parameter refers to different objects
    print(widget1.getValue())
    print(widget2.getValue())

main()