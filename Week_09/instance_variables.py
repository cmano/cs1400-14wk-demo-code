class Widget:
    def __init__(self, value):
        self.value = value # This is an instance variable
        otherValue = value # This is a local variable

    def getInstanceVariable(self):
        # Instance variables are in scope throughout the class
        return self.value

    def getLocalVariable(self):
        # Local variables are only in scope in the function
        # So this wouldn't work
        return otherValue
