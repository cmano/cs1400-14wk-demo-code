name = "Chris"
age = 25
color = "red"
itemWidth = 22
titleWidth = itemWidth + 5

msg = format("Welcome", "^" + str(titleWidth))
msg += "\n"
msg += format("My name is: ", ">" + str(itemWidth))
msg += name
msg += "\n"
msg += format("My age is: ", ">" + str(itemWidth))
msg += str(age)
msg += "\n"
msg += format("Age next year: ", ">" + str(itemWidth))
newAge = age + 1
msg += str(newAge)
msg += "\n"
msg += format("My favorite Color is: ", ">" + str(itemWidth))
msg += color
msg += "\n"


print(msg)