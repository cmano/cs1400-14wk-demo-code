str1 = "Hello"

# The format definition is just a string
formatDef = ""
formatDef += ">"

spacing = input("How much space do you want? ")
formatDef += spacing

# The return of format is a string. You can then do anything else you can with a string
result = format(str1, formatDef)
print(result + "The End")

# format() does not change the original string
print(str1)