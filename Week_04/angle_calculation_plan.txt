Software Development Plan
1) Requirements Specification
    Prompt the user for three points. Calculate and display the angles formed
    by a triangle with the points

2) System Analysis
    a ^ 2 = b ^ 2 + c ^ 2

    angle A = arccosine   side A ^ 2 - side B ^ 2 - side C ^2
                          -----------------------------------
                          -2 * side B * side C

    Convert radians degrees

3) System Design
    Prompt user to enter point 1 in x, y format
    Convert to numbers and save
    Prompt user to enter point 2 in x, y format
    Convert to numbers and save
    Prompt user to enter point 3 in x, y format
    Convert to numbers and save
    Calculate and save the length of side a using pythagorean theorem
    Calculate and save the length of side b using pythagorean theorem
    Calculate and save the length of side c using pythagorean theorem
    Calculate and save the angle A using formula above
    Convert angle A from radians to degrees
    Calculate and save the angle B using formula above
    Convert angle B from radians to degrees
    Calculate and save the angle C using formula above
    Convert angle C from radians to degrees
    Display the values of the three angles rounded to two places

4) Testing
    Test 1
        Inputs
            1, 1
            6.5, 1
            6.5, 2.5
        Output
            15.26
            90.0
            74.74
    Test 2
        Inputs
            30, 20
            18, 4
            21, 98
        Output
            136.55
            35.04
            8.41