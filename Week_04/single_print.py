msg = "This"
msg += " "
msg += "a"
msg += " way I "
msg += "can build up a string "
msg += "and then only"
msg += " "
msg += "call print() one time"

print(msg)

# Escape sequences and formatting work too!
msg = "I didn't use the concatenation operator on this line"
msg += "\nI can concatenate an escape sequence to get a new line"
msg += "\n"
msg += format("I can format too: ", ">35s")
msg += format(123.45678, "<10.2f")
msg += "\n"
msg += format("It's easy to line up numbers: ", ">35s")
msg += format(123.45678, "<10.2f")
msg += "\n"
msg += format("And edit if I need to change: ", ">35s")
msg += format(123.45678, "<10.2f")
print(msg)

# This is really difficult to read and to edit
print("I didn't use the concatenation operator on this line\nI can concatenate an escape sequence to get a new line\n" + format("I can format too: ", ">35s") +
      format(123.45678, "<10.2f") + "\n" + format("It's easy to line up numbers: ", ">35s") + format(123.45678, "<10.2f") + "\n" +
      format("And edit if I need to change: ", ">35s") + format(123.45678, "<10.2f"))
