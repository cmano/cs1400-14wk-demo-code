# Import statements should go at the top of the file
import math # Import a single module to access all items in the module
from math import pi # Import an item from a module if you're using it many times

print("The value of PI is", pi) # Just use pi because it was imported
print("The value of PI is still", pi)
print("The value of PI is still", pi)
print("I'm only using pow once", math.pow(5, 2)) # Need to use math.pow because math was imported, but pow wasn't