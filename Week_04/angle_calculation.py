from math import acos
import math

# Get the points from the user
x1, y1 = eval(input("Enter the first point. eg. x, y: "))
x2, y2 = eval(input("Enter the second point. eg. x, y: "))
x3, y3 = eval(input("Enter the third point. eg. x, y: "))

# Calculate side lengths
sideA = math.sqrt((x2 - x3) ** 2 + (y2 - y3) ** 2)
sideB = math.sqrt((x1 - x3) ** 2 + (y1 - y3) ** 2)
sideC = math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

# Calculate angle values
angleA = math.degrees(acos(((sideA * sideA - sideB * sideB - sideC * sideC) / (-2 * sideB * sideC))))
angleB = math.degrees(acos(((sideB * sideB - sideA * sideA - sideC * sideC) / (-2 * sideA * sideC))))
angleC = math.degrees(acos(((sideC * sideC - sideB * sideB - sideA * sideA) / (-2 * sideB * sideA))))

# Display the angles
print("The three angles are ", round(angleA, 2), round(angleB, 2), round(angleC, 2))