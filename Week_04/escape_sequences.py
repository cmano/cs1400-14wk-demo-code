# Escape sequences
print("\tThis is a tab")
print("This is a\nnew line")
print("This is a\n\tnew line with a tab")
print("This as \"a quote\"")
print("This uses a \\")
print("This uses a\rcarriage return")
print("This uses a backspace so you can correct erroes\b\brs" +
      " in the most awkward way possible")

print()
msg = "This is a repeat of the previous code, but with concatenation\n"
msg += "\tThis is a tab\n"
msg += "This is a\nnew line\n"
msg += "This is a\n\tnew line with a tab\n"
msg += "This as \"a quote\"\n"
msg += "This uses a \\\n"
msg += "This uses a\rcarriage return\n"
msg += "This uses a backspace so you can correct erroes\b\brs" + \
       " in the most awkward way possible"
print(msg)


print()
print("Tabs are useful for a menu")
print("\t1) You can indent the options")
print("\t2) This makes it easy to read")
print("\t3) At the end the user can...")
choice = input("Make a choice(1-3): ")