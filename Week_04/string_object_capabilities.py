# Check out the documentation at python.org for all of the string methods/capabilities
# Library Reference > Built-In Types > Text Sequence Type
name = "Aggies"

print(name.upper())
print(name.lower())
print(name.isdigit())
print(name.islower())