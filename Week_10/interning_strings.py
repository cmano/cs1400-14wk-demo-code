
print("Repeat 4 Times")
str1 = "Hello" * 4
str2 = "Hello" * 4

print("   == :", str1 == str2)
print("id == :", id(str1) == id(str2))
print("   is :", str1 is str2)

print("\nNow Repeat 800 Times")
str1 = "Hello" * 820
str2 = "Hello" * 820

print("   == :", str1 == str2)
print("id == :", id(str1) == id(str2))
print("   is :", str1 is str2)

print("\nMake sure you're comparing what you want to")