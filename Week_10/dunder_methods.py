from modules.circle import Circle

def main():
    cir1 = Circle(10)
    cir2 = Circle(10)

    print("Add")
    print([cir1 + cir2])

    print("\nSubtract")
    print(cir1 - cir2)

    print("\nLess Than")
    print(cir1 < cir2)

    print("\nGreater Than")
    print(cir1 > cir2)

    print("\nLess Than or Equal")
    print(cir1 <= cir2)

    print("\nGreater Than or Equal")
    print(cir1 >= cir2)

    print("\nEqual")
    print(cir1 == cir2)

    print("\nNot Equal")
    print(cir1 != cir2)

    print("\nLength")
    print(len(cir1))

main()