from math import pi

class Circle:
    def __init__(self, radius):
        self.__radius = radius

    def getRadius(self):
        return self.__radius

    def __getArea(self):
        return pi * self.__radius ** 2

    def __getCircumference(self):
        return pi * self.__radius * 2

    def __add__(self, other):
        return Circle(self.__radius + other.getRadius())

    def __sub__(self, other):
        return Circle(abs(self.__radius - other.getRadius()))

    # defining __lt__ also defines __gt__
    def __lt__(self, other):
        return self.__radius < other.getRadius()

    # defining __le__ also defines __ge__
    def __le__(self, other):
        return self.__radius <= other.getRadius()

    # defining __eq__ also defines __ne__
    # If you do not define this == acts like the 'is' operator
    def __eq__(self, other):
        return self.__radius == other.getRadius()

    def __len__(self):
        return int(self.__getCircumference())

    def __str__(self):
        return "I am a Circle with radius " + str(self.__radius)

    def __repr__(self):
        return "Area: " + str(self.__getArea())