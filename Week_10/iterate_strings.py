def main():
    val1 = "Hello Goodbye"

    print("Iterate by Index")
    for i in range(len(val1)):
        print(val1[i])

    print()
    print("Iterate by Index and Access Neighbor")
    for i in range(len(val1) - 1):
        print(val1[i] + val1[i + 1])

    print()
    print("Iterate by Item")
    for i in val1:
        print(i)
    print("Note: No way to access neighbor")


main()