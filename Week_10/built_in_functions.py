def main():
    str1 = "StringDemo"

    print("Length:", len(str1))
    print("Max:", max(str1)) # Remember Uppercase letters come before Lowercase
    print("Min:", min(str1))

main()