import random

def main():
    lst = []

    for i in range(30):
        lst.append(random.randint(100, 999))

    print(lst)
    insertionSort(lst)
    print(lst)


def insertionSort(inputList):
    for i in range(1, len(inputList)):
        currElement = inputList[i]
        j = i - 1
        while j >= 0 and inputList[j] > currElement:
            inputList[j + 1] = inputList[j]
            j -= 1

        inputList[j + 1] = currElement


main()