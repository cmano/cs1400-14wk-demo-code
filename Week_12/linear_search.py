def main():
    # List does not have to be in order
    list1 = [4, 6, 1, 43, 1, 76, 85, 30, 8, 29]
    key = 85
    result = linearSearch(list1, key)
    if result == -1:
        print("Did not find", key)
    else:
        print("Found,", key, "at position", result)


def linearSearch(inputList, key):
    for i in range(len(inputList)):
        if key == inputList[i]:
            return i
    return -1


main()