def main():
    # List needs to be in order
    list1 = [1, 4, 6, 8, 29, 30, 43, 76, 85]
    key = 43
    result = binarySearch(list1, key)
    if result == -1:
        print("Did not find", key)
    else:
        print("Found,", key, "at position", result)


def binarySearch(inputList, key):
    print(inputList)
    low = 0
    high = len(inputList) - 1
    while high >= low:
        print(low, high)
        mid = (high + low) // 2
        if key == inputList[mid]:
            return mid
        elif key < inputList[mid]:
            high = mid - 1
        else:
            low = mid + 1

    return -1


main()