import random

def main():
    lst = []

    for i in range(30):
        lst.append(random.randint(100, 999))

    print(lst)
    selectionSort(lst)
    print(lst)


def selectionSort(inputList):
    for i in range(len(inputList) - 1):
        currMinIndex = i

        for j in range(i + 1, len(inputList)):
            if inputList[currMinIndex] > inputList[j]:
                currMinIndex = j

        if currMinIndex != i:
            inputList[i], inputList[currMinIndex] = inputList[currMinIndex], inputList[i]


main()