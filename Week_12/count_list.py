def main():
    str1 = "ancnklweldalksdfolnqpqwenzvlldifalkdflnqlasdliasfnla"

    counterList = [0] * 26

    for ch in str1:
        # ord() converts a letter to its ASCII/Unicode value
        counterList[ord(ch) - ord("a")] += 1

    for i in range(len(counterList)):
        print(chr(i + ord("a")) + ": " + str(counterList[i]))

main()