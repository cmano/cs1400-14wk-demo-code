import random

def main():
    lst = []

    for i in range(30):
        lst.append(random.randint(100, 999))

    print(lst)
    bubbleSort(lst)
    print(lst)


def bubbleSort(inputList):
    didSwap = True

    while didSwap:
        didSwap = False
        sortCnt = 1

        for i in range(len(inputList) - sortCnt):
            if inputList[i] > inputList[i + 1]:
                inputList[i], inputList[i + 1] = inputList[i + 1], inputList[i]
                didSwap = True

        sortCnt += 1


main()