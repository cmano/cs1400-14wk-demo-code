import random

# Generate two random numbers
num1 = random.randint(0, 99)
num2 = random.randint(0, 99)

# Display problem and prompt user for answer
print(str(num1) + " + " + str(num2) + " =")
userAnswer = eval(input("What is your answer? "))

# Calculate actual answer
actualAnswer = num1 + num2

# Compare answer
correct = userAnswer == actualAnswer

# Display message to user
print(str(num1) + " + " + str(num2) + " = " + str(userAnswer) + " is " + str(correct))