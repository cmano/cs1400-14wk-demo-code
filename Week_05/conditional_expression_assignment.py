val1 = 5

print("Long way")
if val1 > 0:
    num1 = 1
else:
    num1 = -1
print(num1)

print()
print("Conditional Expression")
num2 = 1 if val1 > 0 else -1
print(num2)