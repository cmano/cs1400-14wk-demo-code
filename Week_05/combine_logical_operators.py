number = eval(input("Enter a number: "))

twoOrFive = number % 2 == 0 or number % 5 == 0

print("Two or Five:", twoOrFive)

twoAndFive = number % 2 == 0 and number % 5 == 0

print("Two and Five:", twoAndFive)

twoOrFiveAndNotTwoAndFive = twoOrFive and not twoAndFive

print("Combined: ", twoOrFiveAndNotTwoAndFive)

result = (number % 2 == 0 or number % 5 == 0) and not (number % 2 == 0 and number % 5 == 0)

print("Single Statement:", result)