import random

# Psuedorandom number generators use a seed value
# Only use this if you want the sequence to always be the same
random.seed(100) # Remove this line and try again

# Floating point number [0.0, 1.0)
print(random.random() * 100)

# Integers from [a, b]
print(random.randint(1, 10))
