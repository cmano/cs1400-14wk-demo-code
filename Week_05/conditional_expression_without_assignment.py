val1 = 5

print("Long way")
if val1 % 10 == 0:
    print("multiple of 10")
else:
    print("not a multiple of 10")

print()
print("Conditional Expression Way")
print("multiple of 10") if val1 % 10 == 0 else print("not a multiple of 10")