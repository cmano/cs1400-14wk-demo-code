def main():
    print(withReturnValue())
    print(noReturn())
    print(noValueReturn())

def withReturnValue():
    print("I will return a value")
    return 20

def noReturn():
    print("I do not return anything")

def noValueReturn():
    print("I return without a value")
    return

main()