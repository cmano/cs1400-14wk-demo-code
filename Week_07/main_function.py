# We're going to start our programs in a function called main()

def main():
    print("This is the starting point of our program")


# Call main to get the program started
# All other code should be inside a function
main()