def maxVal(val1, val2):
    print("maxVal() was called")
    if val1 < val2:
        print("maxVal() about to return")
        return val2
    else:
        print("maxVal() about to return")
        return val1

def main():
    print("main() was called")
    num1 = 5
    num2 = 10

    print("About to call maxVal()")
    maxNum = maxVal(num1, num2)
    print("maxVal() returned")

    print("The maximum value is " + str(maxNum))


print("Calling main()")
main()
print("main() is finished")