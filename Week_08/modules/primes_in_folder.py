def isPrime(num):
    divisor = 2
    while divisor <= num / 2:
        if num % divisor == 0:
            return False
        divisor += 1

    return True

def printPrimes(numToPrint):
    count = 0
    testNum = 2
    NUM_PER_LINE = 10

    while count < numToPrint:
        if isPrime(testNum):
            count += 1
            print(testNum, end=" ")
            if count % NUM_PER_LINE == 0:
                print()
        testNum += 1
