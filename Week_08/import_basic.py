# This might show an error because in my demo code, I have all code organized into folders.
# If I used the Unit and Section folder names the error would disappear
# However, the code still works. The IDE is trying to help out identifying an issue
import modules.primes_in_folder

print(modules.primes_in_folder.isPrime(50))
modules.primes_in_folder.printPrimes(45)