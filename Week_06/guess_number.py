import random

number = random.randint(0, 100)

print("Guess a number from 0 to 100")

guess = -1

while guess != number:
    guess = eval(input("What is your guess? "))

    if guess == number:
        print("You got it!!!")
    elif guess < number:
        print("Too low")
    else:
        print("Too high")