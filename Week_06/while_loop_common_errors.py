count = 0

# This is an infinite loop
# Don't forget the loop control statement!
while count <= 100:
    print(str(count) + ") great message")



# This is an off by one error
# I wanted to print 100
count = 0

while count < 100:
    print(count)
