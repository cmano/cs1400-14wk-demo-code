# Nested loops execute the entire inside loop for each iteration of the outside loop
for i in range(10):
    for j in range(20, 30):
        print(i, j)