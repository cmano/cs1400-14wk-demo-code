# while <loop continuation condition>:
#   <statements>
#   <loop control statement> # This can go anywhere, but needs to be in the loop

import random

total = 0

while total < 200:
    randNum = random.randint(1, 10)
    total += randNum
    print("Added " + str(randNum) + ", and the new total is " + str(total))