# for <variable> in <sequence>:
#   <statements>

# Use range() to create the sequence

# range(a)
print("range(a) Example")
for i in range(10):
    print(i)

# range(a, b)
print("\nrange(a, b) Example")
for i in range(10, 20):
    print(i)

# range(a, b, c)
print("\nrange(a, b, c) Example")
for i in range(10, 20, 2):
    print(i)

# range(a, b, -c)
print("\nrange(a, b, -c) Example")
for i in range(20, 10, -2):
    print(i)