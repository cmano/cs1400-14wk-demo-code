valid = False

while not valid:
    num = input("Enter a number: ")

    if num.isdigit():
        num = eval(num)
        valid = True
    else:
        print("Invalid input. Try again.")

print("Your number squared is " + str(num ** 2))