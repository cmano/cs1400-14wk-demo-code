# Break terminates the entire loop
print("Break")
for i in range(10):
    for j in range(10):
        if j == 5:
            break
        print(i, j)


# Continue terminates the entire loop
print("Continue")
for i in range(10):
    for j in range(10):
        if j == 5:
            continue
        print(i, j)


# Break and continue apply to the loop that directly contains the break/continue statement
# when loops are nested
print("Nested")
for i in range(10):
    for j in range(10):
        if j == 5:
            continue
        print(i, j)