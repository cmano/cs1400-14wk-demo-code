def main():
    value = eval(input("Enter a value: "))
    print("The Fibonacci of", value, "is", fibonacci(value))

def fibonacci(val):
    print("Calling fibonacci(" + str(val) + ")")
    if val == 0:
        return 0
    elif val == 1:
        return 1
    else:
        return fibonacci(val - 1) + fibonacci(val - 2)

main()