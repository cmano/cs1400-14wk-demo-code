from random import randint
from random import seed

def main():
    seed(0) # Keep the same order of random numbers
    myList = []
    sumInnerList = []
    sumColVal = []

    # Fill with random numbers
    for i in range(10):
        myList.append([])
        for j in range(5):
            myList[i].append(randint(10, 99))

    # sum each inner list
    for i in range(len(myList)):
        sum = 0
        for j in range(len(myList[i])):
            sum += myList[i][j]

        sumInnerList.append(sum)

    print(sumInnerList)

    # Sum each value for a specific index in each inner list
    for i in range(len(myList[0])):
        sum = 0
        for j in range(len(myList)):
            sum += myList[j][i]

        sumColVal.append(sum)

    print(sumColVal)



main()