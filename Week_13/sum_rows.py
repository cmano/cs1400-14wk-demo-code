import random


def main():
    # Create a two dimensional list of random numbers
    list1 = []
    for i in range(10):
        list1.append([])
        for j in range(10):
            list1[i].append(random.randint(0, 10))
    print(list1)
    print("The sum of all: " + str(sumAll(list1)))

    maxRow, maxSum = maxRowSum(list1)
    print("Row " + str(maxRow) + " has the max sum of " + str(maxSum))

    maxCol, maxSum = maxColSum(list1)
    print("Column " + str(maxCol) + " has the max sum of " + str(maxSum))


def sumAll(lst):
    sum = 0

    for row in lst:
        for num in row:
            sum += num

    return sum

def maxRowSum(lst):
    currMaxRow = -1
    currMaxSum = -1
    for i in range(len(lst)):
        rowSum = 0
        for j in range(len(lst[i])):
            rowSum += lst[i][j]

        if rowSum > currMaxSum:
            currMaxSum = rowSum
            currMaxRow = i

    return currMaxRow, currMaxSum


def maxColSum(lst):
    currMaxCol = -1
    currMaxSum = -1
    for j in range(len(lst[0])):
        colSum = 0
        for i in range(len(lst)):
            colSum += lst[i][j]

        if colSum > currMaxSum:
            currMaxSum = colSum
            currMaxCol = j

    return currMaxCol, currMaxSum

main()