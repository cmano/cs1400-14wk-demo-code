from deck import Deck


def main():
    hands = []

    numPlayers = eval(input("How many players? "))

    for i in range(numPlayers):
        hands.append([])

    numHands = eval(input("How many hands? "))

    for i in hands:
        for j in range(numHands):
            i.append([])

    cardCnt = eval(input("How many cards? "))

    deck = Deck()

    for i in range(numHands):
        for j in range(cardCnt):
            for k in range(numPlayers):
                hands[k][i].append(deck.draw())

                for l in hands:
                    print(l)


main()