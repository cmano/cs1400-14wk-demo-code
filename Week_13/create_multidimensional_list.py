import random

def main():
    # Create a list with two empty lists
    list1 = [[],[]]
    print("A list of two empty lists")
    print(list1)
    print()

    # Create an empty list then create 10 empty lists inside it
    list2 = []

    for i in range(10):
        list2.append([])
    print("A list appended with 10 empty lists")
    print(list2)
    print()

    # Fill the lists inside list2 with random numbers
    for i in range(len(list2)):
        for j in range(10):
            list2[i].append(random.randint(0, 9))
    print("A list of lists filled with random numbers")
    print(list2)


main()