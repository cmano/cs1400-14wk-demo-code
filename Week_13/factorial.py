def main():
    value = eval(input("Enter a value: "))
    print("The Factorial of", value, "is", factorial(value))

def factorial(val):
    print("Calling factorial(" + str(val) + ")")
    if val == 0:
        return 1
    else:
        return val * factorial(val -1)

main()