from random import randint

def main():
    numList = []

    for i in range(100):
        numList.append(randint(1, 200))

    numList.sort()

    print(binarySearch(numList, 87))

def binarySearch(lst, key):
    low = 0
    high = len(lst) - 1
    return binarySearchRecursive(lst, key, low, high)

def binarySearchRecursive(lst, key, low, high):
    print("Recursive call: ", low, high)
    if low > high:
        return -1

    mid = (low + high) // 2
    if key == lst[mid]:
        return mid
    elif key < lst[mid]:
        return binarySearchRecursive(lst, key, low, mid - 1)
    else:
        return binarySearchRecursive(lst, key, mid + 1, high)

main()