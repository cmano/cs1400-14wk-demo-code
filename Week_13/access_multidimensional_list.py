import random

def main():
    # Create a two dimensional list of random numbers
    list1 = []
    for i in range(5):
        list1.append([])
        for j in range(10):
            list1[i].append(random.randint(0, 9))

    #Access by index
    print("Access by Index")
    for i in range(len(list1)):
        for j in range(len(list1[i])):
            print(list1[i][j], end=" ")
        print()

    #Access by element
    print("\nAccess by Element")
    for row in list1:
        for num in row:
            print(num, end=" ")
        print()

main()