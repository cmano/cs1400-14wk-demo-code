def main():
    students = ["Alice", "Bob", "Charlie"]
    subjects = ["English", "Math", "Science"]

    grades = [[[80, 75, 90],
               [85, 90, 92],
               [90, 75, 80]],
              [[99, 50, 80],
               [90, 95, 80],
               [80, 95, 85]],
              [[75, 80, 90],
               [90, 75, 99],
               [95, 89, 75]]]

    studentNum = 1
    subjectNum = 2
    assnNum = 0

    print(students[studentNum] + ", " + subjects[subjectNum] + ", " + str(assnNum) + ", " + str(grades[studentNum][subjectNum][assnNum]))

main()