def main():
    list0 = list(range(10))
    print(list0)

    list1 = [x for x in list0]
    print(list1)

    print()
    list2 = [0.5 * x for x in list0]
    print(list2)

    print()
    list3 = [0.5 * x for x in list0 if x < 4]
    print(list3)

main()