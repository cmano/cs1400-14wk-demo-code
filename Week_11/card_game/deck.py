import random
from card import Card
from card_map import RANKS
from card_map import SUITS

class Deck:
    def __init__(self):
        self.__deck = []

    def shuffle(self):
        self.__deck.clear()
        for i in range(len(RANKS) * len(SUITS)):
            self.__deck.append(Card(i))
        random.shuffle(self.__deck)

    def draw(self):
        return self.__deck.pop()
