from card_map import RANKS
from card_map import SUITS

class Card:
    def __init__(self, id):
        self.__id = id

    def getRank(self):
        return RANKS[self.__id % len(RANKS)]

    def getSuit(self):
        return SUITS[self.__id // len(RANKS)]

    def getCardId(self):
        return self.__id

    def getCardValue(self):
        return self.__id % len(RANKS) + 1
    # Dunder to return a string representation
    def __repr__(self):
        return self.getRank() + " of " + self.getSuit()

