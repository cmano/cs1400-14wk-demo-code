# putting each item of a list on a new line makes it easier to
# read if there are many elements

SUITS = ["Spades",
         "Clubs",
         "Hearts",
         "Diamonds",
         "Triangles"]

RANKS = ["Ace",
         "Two",
         "Three",
         "Four",
         "Five",
         "Six",
         "Seven",
         "Eight",
         "Nine",
         "Ten",
         "Jack",
         "Queen",
         "King",
         "Assistant to the Regional Manager"]
