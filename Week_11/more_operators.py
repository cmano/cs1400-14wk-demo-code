def main():
    list1 = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    print("Original List")
    print("\t" + str(list1))
    print()

    print("Split")
    print("\t[3:8] - " + str(list1[3:8]))
    print("\t[3:8:2] - " + str(list1[3:8:2]))
    print("\t[8:2:-2] - " + str(list1[8:2:-2]))
    print()

    print("in/not in")
    print("\t5 in: " + str(5 in list1))
    print("\t5 not in: " + str(5 not in list1))
    print("\t13 in: " + str(13 in list1))
    print("\t13 not in: " + str(13 not in list1))
    print()

    print("for loop/traverse a list")
    for i in list1:
        print("\tElement: " + str(i))
    print()

    print("Comparisons")
    # Change the values in the list to see results
    listA = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    listB = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    print("\t <: " + str(listA < listB))
    print("\t<=: " + str(listA <= listB))
    print("\t >: " + str(listA > listB))
    print("\t>=: " + str(listA >= listB))
    print("\t==: " + str(listA == listB))
    print("\t!=: " + str(listA != listB))

main()