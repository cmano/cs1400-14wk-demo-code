from random import shuffle

def main():
    list1 = [1, 5, 3, 2, 6, 8, 6, 9, 4, 7]

    print("len(): " + str(len(list1)))
    print("min(): " + str(min(list1)))
    print("max(): " + str(max(list1)))

    print()
    print("Before Shuffle")
    print("\t" + str(list1))
    shuffle(list1)
    print("After Shuffle")
    print("\t" + str(list1))


main()