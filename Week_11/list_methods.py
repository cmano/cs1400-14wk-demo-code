list1 = [1, 2, 3, 4, 3, 2, 1]
list2 = ["Apple", "Banana", "Cantaloupe"]

print(list1)
print(list2)
print()

print("count()")
print("\tcount of 3's: " + str(list1.count(3)))
print()

print("extend()")
list1.extend(list2)
print("\t" + str(list1))
print()

print("index()")
print("\tApple position: " + str(list1.index("Apple")))
print()

print("insert()")
list1.insert(5, 99)
print("\tinserted 99 at position 5: " + str(list1))
print()

print("pop()")
print("Before: " + str(list1))
list1.pop()
print("After pop(): " + str(list1))
print()

print("pop() with parameter")
print("Before: " + str(list1))
list1.pop(7)
print("After pop(7): " + str(list1))
print()

print("remove()")
print("Before: " + str(list1))
list1.remove("Apple")
print("After remove(\"Apple\"): " + str(list1))
print()

print("sort()")
list1 = [3, 6, 4, 1, 7, 2, 9, 8, 5]
print("Before: " + str(list1))
list1.sort()
print("After: " + str(list1))
print()

